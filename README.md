# Beancount importer for Wealthfront QFX files

[Beancount](http://furius.ca/beancount/) is a text-file-based double-entry accounting language and system.
This project is an [importer](https://beancount.github.io/docs/importing_external_data.html) for [Wealthfront](https://www.wealthfront.com/) QFX files.

## Setup

To use this module, you will need the `ofxparse` library, which can be installed through pip:

    pip install ofxparse

Clone to your `importers` directory, sample folder structure:

    .
    |-- config.py
    |-- importers
    |   |-- __init__.py
    |   `-- wealthfront      # This repository
    |       |-- __init__.py
    `-- personal.beancount

Import the importer and add to your `CONFIG` for beancount import config:

    from importers.wealthfront import Wealthfront

    CONFIG = [
        Wealthfront(
            'Assets:Investments:Wealthfront',
            'Assets:Cash',
            'Income:PnL',
            'Income:Dividends',
            'Expenses:Fees'),
    ]

Ensure your target beanfile has opened accounts for each commodity:

    2020-01-01 open Assets:Investments:Wealthfront:MUB MUB "FIFO"
    2020-01-01 open Assets:Investments:Wealthfront:VTI VTI "FIFO"
    ; ...
    2020-01-01 open Assets:Investments:Wealthfront:TIMXX USD

*"FIFO" lot-resolution method can automatically resolve the empty `{}` lots used in stock sales.

**TIMXX is the money market account used by Wealthfront.
It maintains a 1:1 ratio with the USD, so this is used as the primary transfer/cash flow account.

## Usage

Use with standard `bean-identify`, `bean-extract`, `bean-file` commands, e.g.

    export PYTHONPATH=.
    bean-identify config.py ~/Downloads[/WealthfrontQuickenExport.QFX]
    bean-extract  config.py ~/Downloads[/WealthfrontQuickenExport.QFX]
    bean-file     config.py ~/Downloads[/WealthfrontQuickenExport.QFX] -o file/

## Test

To run tests:

    PYTHONPATH=. pytest

To [re]generate importer extracts

    PYTHONPATH=. pytest --generate
