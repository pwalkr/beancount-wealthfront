"""Test module for Wealthfront QFX beancount importer"""

from tempfile import mkstemp
import os
import unittest

from beancount.ingest import regression_pytest
from __init__ import Wealthfront


account_root = 'Assets:Wealthfront'
account_cash = 'Assets:Cash'
account_gains = 'Income:Gains'
account_dividends = 'Income:Dividends'
account_fees = 'Expenses:Fees'
importer = Wealthfront(account_root, account_cash, account_gains, account_dividends, account_fees)
testdir = os.path.dirname(__file__) + '/wealthfront_test.d'


@regression_pytest.with_importer(importer)
@regression_pytest.with_testdir(testdir)
class TestWealthfront(regression_pytest.ImporterTestBase):
    """Test importer using beancount ingest regression testing"""


class TestOutputBeanfile():
    """Test that importer output passes bean-check"""
    def test_check(self):
        tfd, tpath = mkstemp()

        with os.fdopen(tfd, 'w') as outfile:
            outfile.write("""
2000-01-01 open {0}:MUB MUB "FIFO"
2000-01-01 open {0}:VEA VEA "FIFO"
2000-01-01 open {0}:XLE XLE "FIFO"
2000-01-01 open {0}:TIMXX USD
""".format(account_root))
            outfile.write("""
2000-01-01 open {} USD
2000-01-01 open {} USD
2000-01-01 open {} USD
2000-01-01 open {} USD
""".format(account_cash, account_gains, account_dividends, account_fees))
            with open(testdir + '/WealthfrontQuickenExport.QFX.extract') as extract:
                for line in extract:
                    outfile.write(line)

            outfile.flush()
            assert os.system('bean-check "{}"'.format(tpath)) == 0


if __name__ == '__main__':
    unittest.main()
