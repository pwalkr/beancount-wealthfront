"""A beancount importer for Wealthfront QFX files"""

import re
from datetime import timedelta
from os import path
from ofxparse import OfxParser

from beancount.core import data
from beancount.core.amount import Amount
from beancount.core.position import Cost, CostSpec
from beancount.ingest.importer import ImporterProtocol


# pylint: disable=too-many-instance-attributes
class Wealthfront(ImporterProtocol):
    """A beancount importer for Wealthfront QFX files"""

    # Wealthfront currently only supports US residents
    currency = 'USD'

    # "Cash" is actually kept in this account
    moneymarket = "TIMXX"

    securities = {
            "464288414": "MUB",
            "46434G103": "IEMG",
            "74926P688": "TIMXX",
            "78468R721": "TFI",
            "808524102": "SCHB",
            "808524797": "SCHD",
            "808524805": "SCHF",
            "81369Y506": "XLE",
            "921908844": "VIG",
            "921943858": "VEA",
            "922042858": "VWO",
            "92204A306": "VDE",
            "922907746": "VTEB",
            "922908769": "VTI",
    }

    # 6th argument is optional, and this is how beancount importers work
    # pylint: disable=too-many-arguments
    def __init__(self,
                 account_root,
                 account_cash,
                 account_gains,
                 account_dividends,
                 account_fees):
        self.account_root = account_root
        self.account_cash = account_cash
        self.account_gains = account_gains
        self.account_dividends = account_dividends
        self.account_fees = account_fees
        # Initialize
        self.meta_filename = None
        self.meta_index = 0

    def identify(self, file):
        return re.match(r".*wealthfront.*.qfx$", path.basename(file.name),
                        re.IGNORECASE)

#    def file_name(self, file):
#        pass

    def file_account(self, _):
        return self.account_root

    def file_date(self, file):
        with open(file.name) as seekable:
            ofx = OfxParser.parse(seekable)
            return ofx.account.statement.end_date.date() - timedelta(days=1)

    def get_account(self, ticker=None):
        """Get full account, defaults to money market cash account"""
        if ticker is None:
            ticker = self.moneymarket
        return '{}:{}'.format(self.account_root, ticker)

    # Used in lambda to get date for sorting
    @staticmethod
    def get_date(transaction):
        """Get date from Transaction or InvestmentTransaction object"""

        if type(transaction).__name__ == 'InvestmentTransaction':
            return transaction.tradeDate
        return transaction.date

    def get_meta(self):
        """Get beancount meta object"""
        meta = {'filename': self.meta_filename, 'lineno': self.meta_index}
        self.meta_index += 1
        return meta

    def get_ticker(self, security_id):
        """Get ticker symbol from input security ID"""

        if security_id in self.securities:
            return self.securities[security_id]
        return 'UNKNOWN-' + security_id

    def extract(self, file, existing_entries=None):
        self.meta_filename = file.name
        self.meta_index = 0
        entries = []

        with open(file.name) as seekable:
            ofx = OfxParser.parse(seekable)
            statement = ofx.account.statement

            # Get list of Transaction objects - sorted by date
            entries = self.translate_transactions(statement.transactions)

            # append balances
            entries += self.translate_positions(statement.positions, statement.end_date.date())

        return entries

    def translate_positions(self, positions, date):
        """Convert a list of ofxparse positions into a list of balances"""
        entries = []
        for position in positions:
            ticker = self.get_ticker(position.security)
            if ticker == self.moneymarket:
                unit = self.currency
            else:
                unit = ticker
                # Append a price for non-MM accounts
                # data.Price is callable, this is how the beancount example works
                # pylint: disable=not-callable
                entries.append(data.Price(self.get_meta(), date,
                                          ticker,
                                          Amount(position.unit_price, self.currency)))
            # data.Balance is callable, this is how the beancount example works
            # pylint: disable=not-callable
            entries.append(data.Balance(self.get_meta(), date,
                                        self.get_account(ticker),
                                        Amount(position.units, unit),
                                        None, None))
        return entries

    def translate_transactions(self, transactions):
        """Convert a list of ofxparse transactions into a list of beancount entries"""

        entries = []

        for transaction in transactions:
            narration, postings = self.parse_transaction(transaction)
            if narration is not None:
                # data.Transaction is callable, this is how the beancount example works
                # pylint: disable=not-callable
                entries.append(data.Transaction(
                    self.get_meta(),
                    self.get_date(transaction).date(),  # date
                    self.FLAG,  # flag
                    None,  # payee
                    narration,
                    set(),  # tags
                    set(),  # links
                    postings))

        return entries

    def parse_transaction(self, transaction):
        """Get narration and postings from an ofxparse transaction"""

        if transaction.type == 'income':
            if transaction.income_type != 'DIV':
                raise Exception('Unexpected income_type "{}"'.format(transaction.income_type))

            amount = Amount(transaction.total, self.currency)
            ticker = self.get_ticker(transaction.security)

            return ticker + ' Dividend', [
                data.Posting(self.account_dividends, -amount, None, None, None, None),
                data.Posting(self.get_account(), amount, None, None, None, None)]

        if transaction.type == 'credit':
            amount = Amount(transaction.amount, self.currency)
            return 'Deposit', [
                data.Posting(self.account_cash, -amount, None, None, None, None),
                data.Posting(self.get_account(), amount, None, None, None, None)]

        if transaction.type == 'debit':
            amount = Amount(transaction.amount, self.currency)
            return 'Withdrawal', [
                data.Posting(self.get_account(), amount, None, None, None, None),
                data.Posting(self.account_cash, -amount, None, None, None, None)]

        if transaction.type in ('buystock', 'sellstock'):
            return self.parse_stock(transaction)

        if transaction.type == 'fee':
            amount = Amount(transaction.amount, self.currency)
            return 'Advisory Fee', [
                data.Posting(self.get_account(), amount, None, None, None, None),
                data.Posting(self.account_fees, -amount, None, None, None, None)]

        return None, None

    def parse_stock(self, transaction):
        """Handle buystock and sellstock transactions"""

        ticker = self.get_ticker(transaction.security)
        account_target = self.get_account(ticker)
        units = Amount(transaction.units, ticker)
        price = Amount(transaction.unit_price, self.currency)
        total = Amount(transaction.total, self.currency)

        if transaction.type == 'buystock':
            # Sometimes units * price != total. Beancount has a syntax for this
            if round(transaction.units * transaction.unit_price, 2) == abs(transaction.total):
                cost = Cost(transaction.unit_price, self.currency, None, None)
            else:
                cost = CostSpec(None, abs(transaction.total), self.currency, None, None, None)

            return 'Buy ' + ticker, [
                data.Posting(self.get_account(), total, None, None, None, None),
                data.Posting(account_target, units, cost, price, None, None)]

        if transaction.type == 'sellstock':
            cost = CostSpec(None, None, None, None, None, None)
            return 'Sell ' + ticker, [
                # no cost for sold stock, impossible to match lot here
                data.Posting(account_target, units, cost, price, None, None),
                data.Posting(self.get_account(), total, None, None, None, None),
                data.Posting(self.account_gains, None, None, None, None, None)]

        raise Exception('parse_stock called for non-stock transaction')
