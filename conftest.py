# Source: https://github.com/beancount/beancount/blob/master/examples/ingest/office/importers/conftest.py

# This adds the --generate option.
# pylint: disable=invalid-name
pytest_plugins = "beancount.ingest.regression_pytest"
